FROM openjdk:17-jdk as build
COPY . /app
WORKDIR /app
RUN ./mvnw package

FROM openjdk:17
COPY --from=build /app/target/*.jar /app.jar
CMD java -jar /app.jar
